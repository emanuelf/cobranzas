Rails.application.routes.draw do

  devise_for :users
  root to: "dashboard#index"

  resources :service_types
  resources :services do 
    collection do 
      get 'customers' => 'services#show_customer_view', as: :only_customers
      get 'reportes'
    end
  end
  resources :customers
  resources :zones
  resources :debt_collectors
  resources :companies
  resources :usuarios

  get 'services/:id/planillapagos' => 'services#planilla_pagos', as: :planilla_de_pagos

  resources :dashboard do 
    collection do 
      get 'status' => 'dashboard#estados_de_cuenta'
    end
  end
  resources :fee_payments

  namespace :api do 
    
    post 'services/:id/baja' => 'services#baja'
    get 'services/my_services' => 'services#my_services'
    post 'services/fee_payment' => 'services#fee_payment'

    post 'users/login'
    resources :customers do 
      collection do 
        get 'search'
        get 'my_customers'
        get 'atrasos'
      end
    end
  end

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
