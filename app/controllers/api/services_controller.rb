class Api::ServicesController < Api::ApiController

  def baja
    @service = Service.find(params[:id])
    @service.dar_de_baja(params[:comentarios])
    render json: {message: 'Ok'}, status: :ok
  end

  def my_services
    @services = Service.includes(:customer).where(baja: false, customers: {debt_collector_id: params[:cobrador_id]})
    render json: @services, status: :ok
  end

  def fee_payment
    if params[:fee_payment][:monto].to_f != 0
      FeePayment.create_payments(fee_payment_params)
      render json: {message: 'Ok'}, status: :ok
    else
      render json: {message: 'Problemas'}, status: :unprocessable_entity
    end
  end

  private

    def fee_payment_params
      params.require(:fee_payment).permit(:id, :monto, :fee_id, :debt_collector_id, :comentarios, :fecha_pago)
    end
end
