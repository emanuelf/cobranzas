class Api::CustomersController < Api::ApiController

  def search
    @customers = Customer.search(params[:q]).where(company_id: current_user.company_id)
    render json: @customers, status: :ok, each_serializer: CustomerSearchSerializer
  end

  def my_customers
    @customers = Customer.where(debt_collector_id: params[:cobrador_id]).order(:nombre_completo)
    render json: @customers
  end

  def atrasos
    @customers = Customer.con_mas_de_x_cuotas_adeudadas(1).select { |c| c.debt_collector_id == params[:cobrador_id].to_i}
    render json: @customers
  end
end
