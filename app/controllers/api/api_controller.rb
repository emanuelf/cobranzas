class Api::ApiController < ApplicationController

  skip_before_action :verify_authenticity_token
  skip_before_action :authenticate_user!

  before_action :set_access_control_headers
  after_action :set_access_control_headers

  private
  
    def set_access_control_headers
      headers['Access-Control-Allow-Origin'] = '*'
      headers['Access-Control-Request-Method'] = %w{GET POST OPTIONS}.join(",")
      headers['Access-Control-Allow-Headers']  = 'Content-Type, X-CSRF-Token'
    end

end
