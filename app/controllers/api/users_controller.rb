class Api::UsersController < Api::ApiController

  def login
    @user = AppUser.find_by(username: params[:username], password: params[:password])
    if @user
      render json: @user, status: :ok
    else
      render json: nil, status: 401
    end
  end
end
