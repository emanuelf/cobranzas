class UsuariosController < ApplicationController

  before_action :set_user, only: [:update, :destroy]

  def index
    if current_user.admin?
      @users = User.where(role: 'admin')
    else
      @users = User.where(role: 'admin_compania', company_id: current_user.company_id)
    end
  end

  def new
    @user = User.new  
  end

  def edit
        
  end

  def create
    @user = User.new(user_params)
    if @user.save
      redirect_to usuarios_path
    else
      render :new
    end
  end

  def update
    if @user.update(user_params)
      render users_path, status: :ok
    else
      render :edit, notice: @user.errors
    end
  end

  def destroy
    @user.destroy
    redirect_to usuarios_path, notice: 'Usuario borrado'
  end

  private

    def set_user
      @user = User.find(params[:id])
    end

    def user_params
      params.permit(:email, :password, :password_confirmation, :company_id, :role)
    end
end