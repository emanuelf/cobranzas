include ApplicationHelper
class ServicesController < ApplicationController

  before_action :fix_params_fecha_desde_hasta

  before_action :set_service, only: [:show, :edit, :update, :destroy]
  skip_before_action :authenticate_user!
  before_action :set_service_types, only: [:edit, :new]

  # GET /services
  # GET /services.json
  def index
    if current_user.cobrador?
      @services = Service.includes(:customer).where(customers: {debt_collector_id: current_user.cobrador.id})
    else
      @services = Service.includes(:customer).where(customers: {company_id: current_user.company_id})
    end
    if params[:dados_de_baja]
      @services = @services.where(baja: true)
    else
      @services = @services.where(baja: false)
    end
  end

  def planilla_pagos
    @service = Service.find(params[:id])
    path = Rails.root.join('tmp/' + SecureRandom.hex + '.xlsx')
    Axlsx::Package.new do |p|
      p.workbook.add_worksheet(:name => "Datos") do |sheet|
        sheet.add_row ['Cuota nro',
                       'Monto cuota', 
                       'Fecha pago', 
                       'Pago registrado el', 
                       'Pagado', 
                       'Comentarios']
        @service.fees.each do |fee|
          fee.fee_payments.each do |fp|
            sheet.add_row [ 
              fee.nro, 
              fee.monto.round(2), 
              fp.fecha_pago.to_s,
              l(fp.hora_pago, format: :short),
              fp.monto.round(2),
              fp.comentarios
            ]
          end
        end
      end
      p.serialize(path)
    end
    send_file path, filename: 'reporte.xlsx'
  end

  def reportes
    return unless params[:excel].present? 
    if params[:nro_servicio].present?
      @services = Service.includes(:customer).where(id: params[:nro_servicio], customers: {company_id: current_user.company_id})
    else
      @services = Service.includes(:customer).where(baja: false, customers: {company_id: current_user.company_id})
      @services = @services.where(fecha: params[:desde]..params[:hasta])
      if params[:cobrador_id].present?
        @services = @services.includes(:customer).where(baja: false, customers: {debt_collector_id: params[:cobrador_id]})
      end
      if params[:service_type_id].present?
        @services = @services.where(baja: false, service_type_id: params[:service_type_id])
      end
    end
    path = Rails.root.join('tmp/' + SecureRandom.hex + '.xlsx')
    Axlsx::Package.new do |p|
      p.workbook.add_worksheet(:name => "Datos") do |sheet|
        sheet.add_row ['Cliente', 
                       'DNI', 
                       'Tipo servicio', 
                       'Cobrador', 
                       'Número servicio', 
                       'Fecha', 
                       'Valor servicio', 
                       'Cant. cuotas', 
                       'Valor cuota',
                       'Cuotas pagas',
                       'Coutas pendientes',
                       'Fecha fin',
                       'Pagado',
                       'Adeuda']
        @services.each do |service|
          sheet.add_row [service.customer.to_s, 
            service.customer.dni, 
            service.service_type.nombre, 
            service.customer.debt_collector, 
            service.id, 
            service.fecha.to_s, 
            service.monto_total.round(2), 
            service.fees.count,
            service.fees.first.monto.round(2),
            service.cantidad_cuotas_pagas,
            service.cantidad_cuotas_pendientes,
            service.fees.last.vencimiento, 
            service.pagado.round(2), 
            service.adeudado.round(2)]
        end
      end
      p.serialize(path)
    end
    send_file path, filename: 'reporte.xlsx'
  end

  def show_customer_view
    if params[:token] != customers_token 
      render text: 'NO TIENE PERMISO PARA VER ESTA PÁGINA', status: :ok
    else
      if params[:dni].present?
        @customer = Customer.find_by_dni(params[:dni])
        @services = Service.includes(:customer).where(customers: {dni: params[:dni]})
        flash[:notice] = 'No se encontró este DNI la base de datos' if @customer.nil?
      end
      render :customer_view
    end
  end

  # GET /services/1
  # GET /services/1.json
  def show
  end

  # GET /services/new
  def new
    @service = Service.new
    @cuotas = 3
  end

  # GET /services/1/edit
  def edit
  end

  # POST /services
  # POST /services.json
  def create
    @service = Service.new(service_params)
    respond_to do |format|
      if @service.save_and_gen_fees(fees_params)
        
        format.html { redirect_to services_path, notice: 'Service was successfully created.' }
        format.json { render :show, status: :created, location: @service }
      else
        format.html { render :new }
        format.json { render json: @service.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /services/1
  # PATCH/PUT /services/1.json
  def update
    respond_to do |format|
      @service.periodo_pago = fees_params[:periodo_pago]
      if @service.update(service_params)
        if @service.hay_pagos_hechos? == false
          @service.save_and_gen_fees(fees_params)
        end
        format.html { redirect_to @service, notice: 'Service was successfully updated.' }
        format.json { render :show, status: :ok, location: @service }
      else
        format.html { render :edit }
        format.json { render json: @service.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /services/1
  # DELETE /services/1.json
  def destroy
    @service.dar_de_baja("")
    respond_to do |format|
      format.html { redirect_to services_url, notice: 'Servicio dado de baja' }
      format.json { head :no_content }
    end
  end

  private

    def fix_params_fecha_desde_hasta
      params[:desde] = '2010-01-01' if params[:desde].present? == false
      params[:hasta] = '2040-01-01' if params[:hasta].present? == false
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_service
      @service = Service.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def service_params
      params.require(:service).permit(:fecha, :monto, :customer_id, :descripcion, :comentarios, :service_type_id, :interes)
    end

    def fees_params
      params.require(:fees_params).permit(:periodo_pago, fees: [:nro_cuota, :monto])
    end

    def set_service_types 
      @service_types = ServiceType.all.where(company_id: current_user.company_id)
    end
end
