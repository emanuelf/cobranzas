class CompaniesController < ApplicationController
  before_action :set_company, only: [:show, :edit, :update, :destroy]

  # GET /companies
  # GET /companies.json
  def index
    @companies = Company.all
  end

  # GET /companies/1
  # GET /companies/1.json
  def show
  end

  # GET /companies/new
  def new
    @company = Company.new
    @user    = User.new
  end

  # GET /companies/1/edit
  def edit
    @user = @company.users.first || User.new
  end

  # POST /companies
  # POST /companies.json
  def create
    @company = Company.new(company_params)
    @user = User.new(user_params)
    @user.role = 'admin_compania'
    Company.transaction do 
      if @company.save && @user.save && @user.update(company_id: @company.id)
        redirect_to companies_path, notice: 'Compañia creada' 
      else
        render :new 
      end
    end
  end

  # PATCH/PUT /companies/1
  # PATCH/PUT /companies/1.json
  def update
    if user_params[:id].present?
      @user = User.find(user_params[:id])
      @user.assign_attributes(user_params)
    else
      @user = User.new(user_params) 
      @user.role = 'admin_compania'
    end
    @user.company = @company
    Company.transaction do 
      if @company.update(company_params) && @user.save
        redirect_to companies_path, notice: 'Compañía actualizada.'
      else
        render :edit
      end
    end
  end

  # DELETE /companies/1
  # DELETE /companies/1.json
  def destroy
    @company.destroy
    redirect_to companies_url, notice: 'Compañia destruida.' 
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_company
      @company = Company.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def company_params
      params.require(:company).permit(:nombre)
    end

    def user_params
      params.require(:company).require(:user).permit(:id, :email, :password, :password_confirmation)
    end
end
