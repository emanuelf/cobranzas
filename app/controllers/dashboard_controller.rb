class DashboardController < ApplicationController

  def index
    if current_user.cobrador?
      redirect_to services_path      
    end
    if current_user.admin_compania?
      @services_alertas  = Service.con_mas_de_x_cuotas_adeudadas(3).select {|s| s.customer.company_id == current_user.company_id}
    end
  end
end
