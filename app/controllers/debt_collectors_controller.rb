class DebtCollectorsController < ApplicationController
  before_action :set_debt_collector, only: [:show, :edit, :update, :destroy]

  # GET /debt_collectors
  # GET /debt_collectors.json
  def index
    @debt_collectors = DebtCollector.all.where(company_id: current_user.company_id)
  end

  # GET /debt_collectors/1
  # GET /debt_collectors/1.json
  def show
  end

  # GET /debt_collectors/new
  def new
    @debt_collector = DebtCollector.new
    @debt_collector.build_user
    @debt_collector.build_app_user
  end

  # GET /debt_collectors/1/edit
  def edit
    if @debt_collector.user.nil?
      @debt_collector.build_user      
    end
    if @debt_collector.app_user.nil?
      @debt_collector.build_app_user
    end
  end

  # POST /debt_collectors
  # POST /debt_collectors.json
  def create
    @debt_collector = DebtCollector.new(debt_collector_params)
    @debt_collector.company_id = current_user.company_id

    respond_to do |format|
      if @debt_collector.save
        @debt_collector.user.update(role: 'cobrador', company_id: @debt_collector.company_id)
        format.html { redirect_to @debt_collector, notice: 'Debt collector was successfully created.' }
        format.json { render :show, status: :created, location: @debt_collector }
      else
        format.html { render :new }
        format.json { render json: @debt_collector.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /debt_collectors/1
  # PATCH/PUT /debt_collectors/1.json
  def update
    @debt_collector.company_id = current_user.company_id
    respond_to do |format|
      if @debt_collector.update(debt_collector_params)
        @debt_collector.user.update(role: 'cobrador', company_id: @debt_collector.company_id)
        format.html { redirect_to @debt_collector, notice: 'Debt collector was successfully updated.' }
        format.json { render :show, status: :ok, location: @debt_collector }
      else
        format.html { render :edit }
        format.json { render json: @debt_collector.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /debt_collectors/1
  # DELETE /debt_collectors/1.json
  def destroy
    @debt_collector.destroy
    respond_to do |format|
      format.html { redirect_to debt_collectors_url, notice: 'Debt collector was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_debt_collector
      @debt_collector = DebtCollector.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def debt_collector_params
      if params[:debt_collector][:user_attributes][:password].blank? 
        params[:debt_collector][:user_attributes].delete :password
        params[:debt_collector][:user_attributes].delete :password_confirmation
      end

      if params[:debt_collector][:app_user_attributes][:password].blank? 
        params[:debt_collector][:app_user_attributes].delete :password
      end

      params.require(:debt_collector).permit(:apynom, 
        app_user_attributes:[:username, :password, :id, :debt_collector_id],
        user_attributes: [:id, :email, :password, :password_confirmation, :role])

    end
end
