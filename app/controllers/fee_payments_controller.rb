class FeePaymentsController < ApplicationController

  def create
    if params[:fee_payment][:monto].to_f != 0
      FeePayment.create_payments(fee_payment_params)
      redirect_to service_path(Fee.find(fee_payment_params[:fee_id]).service)
    else
      render :new, @fee_payment = FeePayment.new(fee_payment_params)
    end
  end

  def edit
    @fee_payment = FeePayment.find(params[:id])
    render :new
  end

  def update
    @fee_payment = FeePayment.find(params[:id])
    @fee_payment.destroy
    FeePayment.create_payments fee_payment_params
    redirect_to service_path(Fee.find(fee_payment_params[:fee_id]).service)
  end

  def new
    @fee_payment = FeePayment.new(fee_id: params[:fee_id])
  end

  private

    def fee_payment_params
      params.require(:fee_payment).permit(:id, :monto, :fee_id, :debt_collector_id, :comentarios, :fecha_pago)
    end


end
