json.extract! service, :id, :fecha, :monto, :customer_id, :descripcion, :comentarios, :created_at, :updated_at
json.url service_url(service, format: :json)
