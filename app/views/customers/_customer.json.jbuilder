json.extract! customer, :id, :zone_id, :debt_collector_id, :nombre_completo, :dni, :telefono, :otro_telefono, :referencia, :comentarios, :created_at, :updated_at
json.url customer_url(customer, format: :json)
