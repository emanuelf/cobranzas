json.extract! debt_collector, :id, :apynom, :created_at, :updated_at
json.url debt_collector_url(debt_collector, format: :json)
