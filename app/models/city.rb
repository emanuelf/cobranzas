# == Schema Information
#
# Table name: cities
#
#  id         :integer          not null, primary key
#  nombre     :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class City < ActiveRecord::Base

  default_scope {order(nombre: :asc)}

  def to_s
    nombre
  end
end
