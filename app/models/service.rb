# == Schema Information
#
# Table name: services
#
#  id              :integer          not null, primary key
#  fecha           :date
#  monto           :decimal(, )
#  customer_id     :integer
#  descripcion     :string
#  comentarios     :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  service_type_id :integer
#  interes         :decimal(, )
#  periodo_pago    :string
#  baja            :boolean
#  comentario_baja :text
#

class Service < ActiveRecord::Base

  MENSUAL   = 'MENSUAL'
  SEMANAL   = 'SEMANAL'
  DIARIO    = 'DIARIO'
  QUINCENAL = 'QUINCENAL'


  PERIODOS_PAGOS = [MENSUAL, QUINCENAL, SEMANAL, DIARIO]

  belongs_to :service_type
  belongs_to :customer
  has_many :fees, -> {order(nro: :asc)}, dependent: :destroy
  validates_presence_of :fecha, :monto, :customer_id, :service_type_id
  validates_presence_of :periodo_pago
  validates_inclusion_of :periodo_pago, :in => PERIODOS_PAGOS
  
  def mensual?
    periodo_pago == MENSUAL
  end

  def monto_total
    monto.to_f * (interes.to_f + 1) 
  end

  def dar_de_baja(comentarios)
    self.baja = true
    self.comentario_baja = comentarios
    self.save!
  end

  def self.con_mas_de_x_cuotas_adeudadas(x)
    rows = Fee.connection.execute(%{
      SELECT service_id
      FROM fees
      WHERE vencimiento < now() AND ID NOT IN (
        SELECT fee_id 
        FROM fee_payments
      )
      GROUP BY service_id 
      HAVING count(id) >= #{x} ;
    })
    services = rows.map { |row|  Service.find(row['service_id']) }
    services.select { |s| !s.baja }
  end

  def hay_pagos_hechos?
    hay = false
    fees.each { |fee| 
      hay = hay || fee.pagado? 
    }
    hay
  end

  def dias_desde_ultimo_pago
    (Date.today - FeePayment.where(fee_id: fees.pluck(:id)).maximum(:fecha_pago)).to_i
  end

  def cuota_pendiente_actual
    fees.order(:nro => :asc).each { |fee|
      if fee.pagado == 0 && fee.con_saldo?
        return fee
      end
    }
    return nil
  end

  def adeudado
    (monto_total - pagado).round(2)
  end

  def credito_disponible
    fees.last.monto_establecido - fees.last.monto
  end

  def pagado
    pagado = 0
    fees.each { |fee|
      pagado += fee.pagado
    }
    pagado += credito_disponible
    pagado.round(2)
  end

  def cantidad_cuotas_vencidas
    fees_vencidas = []
    fees.each { |fee| 
       fees_vencidas << fee if fee.con_saldo? && fee.vencimiento < Date.today
    }
    fees_vencidas.count
  end

  def cantidad_cuotas_pendientes
    cantidad = 0
    fees.each { |fee| 
      cantidad += 1 if fee.con_saldo?
    }
    cantidad
  end

  def cantidad_cuotas_pagas
    fees.count - cantidad_cuotas_pendientes
  end

  #fees_params = {:periodo_pago, fees: [:nro_cuota, :monto]}
  def save_and_gen_fees(fees_params)
    self.periodo_pago = fees_params[:periodo_pago]
    self.interes = (interes / 100)
    self.baja = false
    save!
    fees.destroy_all
    fees_params[:fees].each_with_index { |fee_data, index|
      fecha_vencimiento = Date.today + (index + 1).weeks
      if (fees_params[:periodo_pago] == MENSUAL) 
        fecha_vencimiento = Date.today + (index + 1).months
      elsif fees_params[:periodo_pago] == DIARIO
        fecha_vencimiento = Date.today + (index + 1).days
      elsif fees_params[:periodo_pago] == QUINCENAL
        fecha_vencimiento = Date.today + ((index + 1) * 15).days
      end
      monto = fee_data[:monto] || self.monto / fees_params[:fees].count
      fee = Fee.find_by(service_id: self.id, nro: fee_data[:nro_cuota])
      if fee
        fee.monto = monto
        fee.monto_establecido = monto
        fee.vencimiento = fecha_vencimiento
        fee.save
      else
        fees << Fee.create(
          monto: monto, 
          monto_establecido: monto,
          nro: fee_data[:nro_cuota],
          vencimiento: fecha_vencimiento
          )
      end
    }
  end

  def to_s
    "#{customer.nombre_completo}, #{fecha.to_s}"
  end
end
