# == Schema Information
#
# Table name: app_users
#
#  id                :integer          not null, primary key
#  username          :string
#  password          :string
#  debt_collector_id :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#

class AppUser < ActiveRecord::Base
  belongs_to :debt_collector

  validates_presence_of :username, :password
end
