# == Schema Information
#
# Table name: service_types
#
#  id         :integer          not null, primary key
#  nombre     :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  company_id :integer
#

class ServiceType < ActiveRecord::Base

  default_scope {order(nombre: :asc)}

  def to_s
    nombre
  end
end
