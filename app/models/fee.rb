# == Schema Information
#
# Table name: fees
#
#  id                :integer          not null, primary key
#  monto             :decimal(, )
#  nro               :integer
#  vencimiento       :date
#  service_id        :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  monto_establecido :decimal(, )
#

class Fee < ActiveRecord::Base

  belongs_to :service
  has_many :fee_payments

  def deuda
    (monto - pagado).round(2)
  end

  alias saldo deuda

  def pagado
    fee_payments.sum(:monto).round(2)
  end

  def pagado?
    pagado > 0
  end

  def con_saldo?
    deuda > 0
  end

  def fechas_pagos
    fee_payments.map { |fp| 
      I18n.l fp.fecha_pago, format: :short 
    }.join(',')
  end

  def horas_pagos
    fee_payments.map { |fp| 
      I18n.l fp.created_at, format: :short 
    }.join(',')
  end

  def comentarios_pagos
    fee_payments.map {|fp| fp.comentarios }.join('\n')
  end
end
