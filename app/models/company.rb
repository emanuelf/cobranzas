# == Schema Information
#
# Table name: companies
#
#  id         :integer          not null, primary key
#  nombre     :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Company < ActiveRecord::Base

  has_many :users

  def to_s
    nombre
  end
end
