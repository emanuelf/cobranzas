# == Schema Information
#
# Table name: customer_images
#
#  id                 :integer          not null, primary key
#  customer_id        :integer
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  image_file_name    :string
#  image_content_type :string
#  image_file_size    :integer
#  image_updated_at   :datetime
#

class CustomerImage < ActiveRecord::Base
  belongs_to :customer

  has_attached_file :image, styles: { medium: "500x500" }
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\z/
end
