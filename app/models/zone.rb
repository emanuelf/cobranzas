# == Schema Information
#
# Table name: zones
#
#  id         :integer          not null, primary key
#  nombre     :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Zone < ActiveRecord::Base

  def to_s
    nombre
  end
end
