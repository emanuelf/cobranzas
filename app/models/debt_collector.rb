# == Schema Information
#
# Table name: debt_collectors
#
#  id         :integer          not null, primary key
#  apynom     :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  company_id :integer
#  user_id    :integer
#

class DebtCollector < ActiveRecord::Base

  belongs_to :user
  has_one :app_user

  accepts_nested_attributes_for :user
  accepts_nested_attributes_for :app_user


  def to_s
    apynom
  end
end
