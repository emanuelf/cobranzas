# == Schema Information
#
# Table name: customers
#
#  id                :integer          not null, primary key
#  debt_collector_id :integer
#  nombre_completo   :string
#  dni               :string
#  telefono          :string(50)
#  otro_telefono     :string(50)
#  referencia        :string
#  comentarios       :text
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  direccion         :string
#  company_id        :integer
#

class Customer < ActiveRecord::Base
  belongs_to :debt_collector
  has_many :services
  has_many :customer_images

  accepts_nested_attributes_for :customer_images

  def self.search(q)
    return Customer.all if !q.present?
    Customer.where("nombre_completo ilike(:q) OR dni ilike(:q)", q: "%#{q}%" )
  end

  def self.con_mas_de_x_cuotas_adeudadas(x)
    Service.con_mas_de_x_cuotas_adeudadas(x).map { |s| s.customer }
  end

  def adeudado
    deuda = 0
    services.each { |ser|
      deuda += ser.adeudado
    }
    deuda.round(2)
  end

  def cantidad_cuotas_pendientes
    cantidad = 0
    services.each { |ser| 
      if ser.baja == false
        cantidad += ser.cantidad_cuotas_pendientes
      end
    }
    cantidad
  end

  def cantidad_cuotas_vencidas
    cantidad = 0
    services.each { |ser| 
      if ser.baja == false
        cantidad += ser.cantidad_cuotas_vencidas
      end
    }
    cantidad
  end

  def to_s
    nombre_completo
  end
end
