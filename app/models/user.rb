# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :inet
#  last_sign_in_ip        :inet
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  company_id             :integer
#  role                   :string(20)
#

class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  validates_presence_of :role
  validates_inclusion_of :role, in: %w{admin admin_compania cobrador}
  has_one :cobrador, class_name: 'DebtCollector', foreign_key: 'user_id'
  belongs_to :company

  def tipo_usuario
    case role
    when 'admin'
      'Administrador'
    when 'admin_compania'
      'Administrador de la compañia'
    when 'cobrador'
      'Cobrador'
    end
  end

  def admin?
    role == 'admin'
  end

  def admin_compania?
    role == 'admin_compania'
  end

  def cobrador?
    role == 'cobrador'
  end

end
