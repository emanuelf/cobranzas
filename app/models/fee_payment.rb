# == Schema Information
#
# Table name: fee_payments
#
#  id                :integer          not null, primary key
#  fee_id            :integer
#  monto             :decimal(, )
#  fecha_hora_pago   :datetime
#  debt_collector_id :integer
#  comentarios       :text
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  hora_pago         :time
#

class FeePayment < ActiveRecord::Base

  belongs_to :fee
  belongs_to :debt_collector

  validates_presence_of :fee_id, :fecha_pago, :monto, :hora_pago

  validates_numericality_of :monto, message: 'Debe ser un número'
  validates_numericality_of :monto, message: 'Los pagos deben ser por montos mayores a cero', greater_than: 0

  before_create :set_hora_pago

  def set_hora_pago
    hora_pago = DateTime.now
  end

  def self.create_payments params
    FeePayment.transaction do
      fee = Fee.find(params[:fee_id])
      credito = params[:monto].to_f
      if credito <= fee.monto 
        fp = FeePayment.create(params)
        fp.update(hora_pago: DateTime.now)
        if credito != fee.monto
          next_fee = fee.service.cuota_pendiente_actual
          if next_fee
            next_fee.update(monto: next_fee.monto + fee.saldo) # el saldo es negativo
          else
            #Quedo debiendo, debo generarle una nueva cuota pendiente
            vencimiento = fee.vencimiento + 1.week
            if fee.service.mensual?
              vencimiento = fee.vencimiento + 1.month
            end
            Fee.create( 
              monto_establecido: fee.saldo,
              nro: fee.nro + 1,
              service_id: fee.service_id, 
              monto: fee.saldo, 
              vencimiento: vencimiento)
          end
        end
      else
        monto_cuota_a_pagar = fee.monto 
        while credito != 0 && fee && credito > fee.monto
          FeePayment.create(monto: monto_cuota_a_pagar,
            comentarios: params[:comentarios],
            debt_collector_id: params[:debt_collector_id],
            fecha_pago: params[:fecha_pago],
            hora_pago: DateTime.now,
            fee: fee
            )
          credito -= monto_cuota_a_pagar
          fee = fee.service.cuota_pendiente_actual
        end
        if credito > 0 && fee
          if credito == fee.monto
            FeePayment.create(
              monto: credito,
              comentarios: params[:comentarios],
              debt_collector_id: params[:debt_collector_id],
              fecha_pago: params[:fecha_pago],
              hora_pago: DateTime.now,
              fee: fee
            )
          else
            fee.update(monto: fee.monto - credito)
          end
        end
      end
    end
  end

  def pagado?
    monto > 0
  end

  def saldo
    fee.monto - monto
  end
  
end
