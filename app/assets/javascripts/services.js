$(function() {

  function date2str(x, y) {
    var z = {
        M: x.getMonth() + 1,
        d: x.getDate(),
        h: x.getHours(),
        m: x.getMinutes(),
        s: x.getSeconds()
    };
    y = y.replace(/(M+|d+|h+|m+|s+)/g, function(v) {
        return ((v.length > 1 ? "0" : "") + eval('z.' + v.slice(-1))).slice(-2)
    });

    return y.replace(/(y+)/g, function(v) {
        return x.getFullYear().toString().slice(-v.length)
    });
  }

  //busqueda de clientes
  $("#search").on('keyup', function(event) {
    var query = $(this).val();
    if(query.length > 2) {
      $.get('/api/customers/search?q=' + query , function(data) {
        var tbody = $("tbody.clientes");
        tbody.html("");
        $.each(data, function(index, customer) {
          tbody.append(
            '<tr data-id="' + customer.id + '">' + 
              "<td class=\"nombre_completo\">" + customer.nombre_completo + "</td>" + 
              "<td>" + customer.direccion + "</td>" + 
              "<td>" + customer.cobrador + "</td>" + 
              "<td><button class=\"seleccionar-cliente btn btn-primary btn-sm glyphicon glyphicon-ok\"> </button></td>" +
            "</tr>"
          )
        });
      });
    }
  });

  //selección de un cliente

  $(document).on('click', 'button.seleccionar-cliente', function(event) {
    $("#service_customer_id").val($(this).parent().parent().data('id'));
    $('label[for="customer_id"]').html($(this).parent().parent().find('.nombre_completo').text());
    $("#modal").modal('hide');
  });

  $('#fees_params_periodo_pago').change(function(event) {
    $("#cantidad_cuotas").trigger('keyup');
  });

  $("#service_monto, #service_interes").on('keyup change', function(event) {
    event.preventDefault();
    var montoServicio = parseFloat($("#service_monto").val());
    if (montoServicio <= 0) {
      var divCuotas = $("div.cuotas");
      divCuotas.html("");
      $('#cantidad_cuotas').attr('disabled', 'true');
      $("#params_fees_params_periodo_pago").attr('disabled', 'true');
      
    } else {
      if (!isNaN(montoServicio)) {
        if ($('#cantidad_cuotas').data('service-pagado') == false) {
          $('#cantidad_cuotas').removeAttr('disabled');
        } else {
          $('#cantidad_cuotas').attr('disabled', 'true');
        }
        $("#fees_params_periodo_pago").removeAttr('disabled');
        $("#cantidad_cuotas").trigger('keyup');
      }
    }
  });

  $('#cantidad_cuotas').on('keyup change', function(event) {
    event.preventDefault();
    var cantidadCuotas = parseInt($(this).val());
    var divCuotas = $("div.cuotas");
    divCuotas.html("");

    var montoServicio = parseFloat($("#service_monto").val());
    var interes = parseFloat($('#service_interes').val());
    if (isNaN(interes)) {
      interes = 0;
    }
    montoServicio = montoServicio * ((interes / 100) + 1);
    $('#service_monto_total').val(montoServicio);
    var montoCuota = Math.round((montoServicio / cantidadCuotas) * 100) / 100;
    var periodo = $('select[name="fees_params[periodo_pago]"]').val();
    var diasMas = cantidadCuotas * 7;
    fechaUltimaCuota = new Date()
    fechaUltimaCuota.setDate(fechaUltimaCuota.getDate() + diasMas);
    if (periodo === 'MENSUAL') {
      fechaUltimaCuota = new Date();
      fechaUltimaCuota.setMonth(fechaUltimaCuota.getMonth() + cantidadCuotas);
    } else if (periodo == 'DIARIO') {
      fechaUltimaCuota = new Date();
      fechaUltimaCuota.setDate(fechaUltimaCuota.getDate() + cantidadCuotas);
    } else if (periodo == 'QUINCENAL') {
      fechaUltimaCuota = new Date();
      fechaUltimaCuota.setDate(fechaUltimaCuota.getDate() + cantidadCuotas * 15);
    }
    $('#fecha_fin_servicio_laal').val(date2str(fechaUltimaCuota, 'yyyy-MM-dd'));
    for (var i = 1; i <= cantidadCuotas; i++) {
      divCuotas.append(
        '<div class="col-md-6">' + 
          '<label for="fees_params[fees][][nro_cuota]">Cuota nro</label>' + 
          '<input name="fees_params[fees][][nro_cuota]" type="number" class="form-control" value="'+ i +'">' + 
        '</div>' + 
        '<div class="col-md-6">' + 
          '<label for="fees_params[fees][][monto]">Monto</label>' + 
          '<input name="fees_params[fees][][monto]" type="number" class="form-control" value="'+montoCuota +'">' +
        '</div>'
      )
    }
  });
  
  $("#service_monto").trigger('keyup');
  

  $(document).on('click', '#confirmar-baja', function(event) {
    var idServicio = $('#service_id').val();
    $.post('/api/services/' + idServicio + '/baja', {comentarios: $('#comentarios-baja').val()})
      .done(function(data) {
        location.href="/services";
      })
      .fail(function (error) {
        alert("Error al dar de baja el servicio")
      })
  });

});