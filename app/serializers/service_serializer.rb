# == Schema Information
#
# Table name: services
#
#  id              :integer          not null, primary key
#  fecha           :date
#  monto           :decimal(, )
#  customer_id     :integer
#  descripcion     :string
#  comentarios     :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  service_type_id :integer
#  interes         :decimal(, )
#  periodo_pago    :string
#  baja            :boolean
#  comentario_baja :text
#

class ServiceSerializer < ActiveModel::Serializer
  attributes(*Service.attribute_names.map(&:to_sym))
  attributes :adeudado, :pagado, :fecha_to_s, :fees_data, :puede_pagar

  belongs_to :customer
  belongs_to :service_type

  def fecha_to_s
    object.fecha.to_s
  end

  def monto
    object.monto_total
  end

  def puede_pagar
    puede = true
    object.fees.each do |f|
      if f.fee_payments.where(fecha_pago: Date.today).any?
        puede = false and break
      end
      break if !puede 
    end
    puede
  end

  def fees_data
    fees_data = []
    object.fees.each do |fee|
      fees_data << {
        id: fee.id,
        cuota_nro: fee.nro,
        monto_establecido: fee.monto_establecido.round(2),
        monto_variable: fee.monto.round(2),
        vencimiento: fee.vencimiento.to_s,
        pagado: fee.pagado.round(2),
        deuda: fee.deuda.round(2),
        saldo: fee.saldo.round(2),
        cuota_a_pagar: (fee == object.cuota_pendiente_actual)
      }
    end
    fees_data
  end

end
