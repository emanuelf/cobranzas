# == Schema Information
#
# Table name: customers
#
#  id                :integer          not null, primary key
#  debt_collector_id :integer
#  nombre_completo   :string
#  dni               :string
#  telefono          :string(50)
#  otro_telefono     :string(50)
#  referencia        :string
#  comentarios       :text
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  direccion         :string
#  company_id        :integer
#

class CustomerSerializer < ActiveModel::Serializer
  attributes(*Customer.attribute_names.map(&:to_sym))
  attributes :adeudado, :cantidad_cuotas_vencidas
end
