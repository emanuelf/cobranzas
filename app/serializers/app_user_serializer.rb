# == Schema Information
#
# Table name: app_users
#
#  id                :integer          not null, primary key
#  username          :string
#  password          :string
#  debt_collector_id :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#

class AppUserSerializer < ActiveModel::Serializer
  attributes :id, :username, :debt_collector_id

end
