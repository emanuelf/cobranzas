class CustomerSearchSerializer < ActiveModel::Serializer
  attributes :id, :nombre_completo, :dni, :telefono, :cobrador, :direccion

  def cobrador
    return "" if object.debt_collector.nil?
    object.debt_collector.apynom
  end
end
