require 'test_helper'

class DebtCollectorsControllerTest < ActionController::TestCase
  setup do
    @debt_collector = debt_collectors(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:debt_collectors)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create debt_collector" do
    assert_difference('DebtCollector.count') do
      post :create, debt_collector: { apynom: @debt_collector.apynom }
    end

    assert_redirected_to debt_collector_path(assigns(:debt_collector))
  end

  test "should show debt_collector" do
    get :show, id: @debt_collector
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @debt_collector
    assert_response :success
  end

  test "should update debt_collector" do
    patch :update, id: @debt_collector, debt_collector: { apynom: @debt_collector.apynom }
    assert_redirected_to debt_collector_path(assigns(:debt_collector))
  end

  test "should destroy debt_collector" do
    assert_difference('DebtCollector.count', -1) do
      delete :destroy, id: @debt_collector
    end

    assert_redirected_to debt_collectors_path
  end
end
