# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170727230851) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "app_users", force: :cascade do |t|
    t.string   "username"
    t.string   "password"
    t.integer  "debt_collector_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  add_index "app_users", ["debt_collector_id"], name: "index_app_users_on_debt_collector_id", using: :btree

  create_table "cities", force: :cascade do |t|
    t.string   "nombre"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "companies", force: :cascade do |t|
    t.string   "nombre"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "customer_images", force: :cascade do |t|
    t.integer  "customer_id"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
  end

  add_index "customer_images", ["customer_id"], name: "index_customer_images_on_customer_id", using: :btree

  create_table "customers", force: :cascade do |t|
    t.integer  "debt_collector_id"
    t.string   "nombre_completo"
    t.string   "dni"
    t.string   "telefono",          limit: 50
    t.string   "otro_telefono",     limit: 50
    t.string   "referencia"
    t.text     "comentarios"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.string   "direccion"
    t.integer  "company_id"
  end

  add_index "customers", ["company_id"], name: "index_customers_on_company_id", using: :btree
  add_index "customers", ["debt_collector_id"], name: "index_customers_on_debt_collector_id", using: :btree

  create_table "debt_collectors", force: :cascade do |t|
    t.string   "apynom"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "company_id"
    t.integer  "user_id"
  end

  add_index "debt_collectors", ["company_id"], name: "index_debt_collectors_on_company_id", using: :btree
  add_index "debt_collectors", ["user_id"], name: "index_debt_collectors_on_user_id", using: :btree

  create_table "fee_payments", force: :cascade do |t|
    t.integer  "fee_id"
    t.decimal  "monto"
    t.date     "fecha_pago"
    t.integer  "debt_collector_id"
    t.text     "comentarios"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.time     "hora_pago"
  end

  add_index "fee_payments", ["debt_collector_id"], name: "index_fee_payments_on_debt_collector_id", using: :btree
  add_index "fee_payments", ["fee_id"], name: "index_fee_payments_on_fee_id", using: :btree

  create_table "fees", force: :cascade do |t|
    t.decimal  "monto"
    t.integer  "nro"
    t.date     "vencimiento"
    t.integer  "service_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.decimal  "monto_establecido"
  end

  add_index "fees", ["service_id"], name: "index_fees_on_service_id", using: :btree

  create_table "service_types", force: :cascade do |t|
    t.string   "nombre"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "company_id"
  end

  add_index "service_types", ["company_id"], name: "index_service_types_on_company_id", using: :btree

  create_table "services", force: :cascade do |t|
    t.date     "fecha"
    t.decimal  "monto"
    t.integer  "customer_id"
    t.string   "descripcion"
    t.string   "comentarios"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.integer  "service_type_id"
    t.decimal  "interes"
    t.string   "periodo_pago"
    t.boolean  "baja",            default: false
    t.text     "comentario_baja"
  end

  add_index "services", ["customer_id"], name: "index_services_on_customer_id", using: :btree
  add_index "services", ["service_type_id"], name: "index_services_on_service_type_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email",                             default: "", null: false
    t.string   "encrypted_password",                default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                     default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                                     null: false
    t.datetime "updated_at",                                     null: false
    t.integer  "company_id"
    t.string   "role",                   limit: 20
  end

  add_index "users", ["company_id"], name: "index_users_on_company_id", using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "zones", force: :cascade do |t|
    t.string   "nombre"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_foreign_key "app_users", "debt_collectors"
  add_foreign_key "customer_images", "customers"
  add_foreign_key "customers", "companies"
  add_foreign_key "customers", "debt_collectors"
  add_foreign_key "debt_collectors", "companies"
  add_foreign_key "debt_collectors", "users"
  add_foreign_key "fee_payments", "debt_collectors"
  add_foreign_key "fee_payments", "fees"
  add_foreign_key "fees", "services"
  add_foreign_key "service_types", "companies"
  add_foreign_key "services", "customers"
  add_foreign_key "services", "service_types"
  add_foreign_key "users", "companies"
end
