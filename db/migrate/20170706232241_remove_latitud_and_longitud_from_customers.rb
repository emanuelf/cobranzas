class RemoveLatitudAndLongitudFromCustomers < ActiveRecord::Migration
  def change
    remove_column :customers, :latitud, :string
    remove_column :customers, :longitud, :string
  end
end
