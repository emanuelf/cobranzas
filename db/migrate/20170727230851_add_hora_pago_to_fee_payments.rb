class AddHoraPagoToFeePayments < ActiveRecord::Migration
  def change
    add_column :fee_payments, :hora_pago, :time
  end
end
