class CreateCustomers < ActiveRecord::Migration
  def change
    create_table :customers do |t|
      t.belongs_to :zone, index: true, foreign_key: true
      t.belongs_to :debt_collector, index: true, foreign_key: true
      t.string :nombre_completo
      t.string :dni
      t.string :telefono, limit: 50
      t.string :otro_telefono, limit: 50
      t.string :referencia
      t.text :comentarios

      t.timestamps null: false
    end
  end
end
