class AddMontoEstablecidoToFees < ActiveRecord::Migration
  def change
    add_column :fees, :monto_establecido, :decimal
  end
end
