class DropCityIdAndZoneIdFromCustomers < ActiveRecord::Migration
  def change
    remove_column :customers, :zone_id
    remove_column :customers, :city_id
  end
end
