class CreateServices < ActiveRecord::Migration
  def change
    create_table :services do |t|
      t.date :fecha
      t.decimal :monto
      t.belongs_to :customer, index: true, foreign_key: true
      t.string :descripcion
      t.string :comentarios

      t.timestamps null: false
    end
  end
end
