class AddBajaAndComentarioBajaToServices < ActiveRecord::Migration
  def change
    add_column :services, :baja, :boolean, default: false
    add_column :services, :comentario_baja, :text
  end
end
