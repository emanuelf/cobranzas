class AddCompanyIdToCustomersDebtCollectorsAndServices < ActiveRecord::Migration
  def change
    add_reference :customers, :company, index: true, foreign_key: true
    add_reference :debt_collectors, :company, index: true, foreign_key: true
  end
end
