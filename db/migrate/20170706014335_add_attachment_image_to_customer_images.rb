class AddAttachmentImageToCustomerImages < ActiveRecord::Migration
  def self.up
    change_table :customer_images do |t|
      t.attachment :image
    end
  end

  def self.down
    remove_attachment :customer_images, :image
  end
end
