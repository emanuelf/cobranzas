class AddLatitudAndLongitudToCustomers < ActiveRecord::Migration
  def change
    add_column :customers, :latitud, :decimal
    add_column :customers, :longitud, :decimal
  end
end
