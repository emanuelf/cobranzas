class CreateAppUsers < ActiveRecord::Migration
  def change
    create_table :app_users do |t|
      t.string :username
      t.string :password
      t.belongs_to :debt_collector, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
