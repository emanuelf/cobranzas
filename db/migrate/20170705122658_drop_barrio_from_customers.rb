class DropBarrioFromCustomers < ActiveRecord::Migration
  def change
    remove_column :customers, :barrio
  end
end
