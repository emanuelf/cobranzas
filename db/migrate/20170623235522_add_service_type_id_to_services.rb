class AddServiceTypeIdToServices < ActiveRecord::Migration
  def change
    add_reference :services, :service_type, index: true, foreign_key: true
  end
end
