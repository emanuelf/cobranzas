class AddUserIdToDebtCollectors < ActiveRecord::Migration
  def change
    add_reference :debt_collectors, :user, index: true, foreign_key: true
  end
end
