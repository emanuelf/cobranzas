class CreateDebtCollectors < ActiveRecord::Migration
  def change
    create_table :debt_collectors do |t|
      t.string :apynom

      t.timestamps null: false
    end
  end
end
