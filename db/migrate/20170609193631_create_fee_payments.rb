class CreateFeePayments < ActiveRecord::Migration
  def change
    create_table :fee_payments do |t|
      t.belongs_to :fee, index: true, foreign_key: true
      t.decimal :monto
      t.date :fecha_pago
      t.belongs_to :debt_collector, index: true, foreign_key: true
      t.text :comentarios

      t.timestamps null: false
    end
  end
end
