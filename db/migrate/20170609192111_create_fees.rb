class CreateFees < ActiveRecord::Migration
  def change
    create_table :fees do |t|
      t.decimal :monto
      t.integer :nro
      t.date :vencimiento
      t.belongs_to :service, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
