class CreateCustomerImages < ActiveRecord::Migration
  def change
    create_table :customer_images do |t|
      t.belongs_to :customer, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
