# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Company.create(nombre: 'Salón de fiestas 1')
Company.create(nombre: 'Salón de fiestas 2')

ServiceType.create(nombre: 'CASAMIENTO')
ServiceType.create(nombre: '15 AÑOS')

services_types_ids = ServiceType.pluck(:id)

companies_ids = Company.pluck(:id)

4.times {
  DebtCollector.create(
    apynom: Faker::Name.unique.name,
    company_id: companies_ids.sample
    )
}

ids_cobradores = DebtCollector.all.pluck(:id)

User.create(email: 'admin@fiestas.com', password: 'salonsalon', password_confirmation: 'salonsalon', role: 'admin', company_id: companies_ids.sample)
User.create(email: 'salon@fiestas.com', password: 'salonsalon', password_confirmation: 'salonsalon', role: 'admin_compania', company_id: companies_ids.sample)
User.create(email: 'cobrador@fiestas.com', password: 'salonsalon', password_confirmation: 'salonsalon', role: 'cobrador', company_id: companies_ids.sample, cobrador: DebtCollector.first)

100.times {
  Customer.create(
    nombre_completo: Faker::Name.unique.name,
    telefono: Faker::PhoneNumber.cell_phone,
    otro_telefono: Faker::PhoneNumber.phone_number,
    direccion: Faker::Address.street_address,
    dni: Faker::Number.number(10).to_s,
    debt_collector_id: ids_cobradores.shuffle[0],
    company_id: companies_ids.sample
    )
}

customers_ids = Customer.all.pluck(:id)

30.times {
  Service.create(
    customer_id: customers_ids.shuffle[0],
    fecha: Faker::Date.between(3.months.ago, Date.today),
    monto: Faker::Number.decimal(5, 2),
    descripcion: Faker::Lorem.sentence,
    comentarios: Faker::Lorem.paragraph, 
    service_type_id: services_types_ids.sample,
    periodo_pago: Service::PERIODOS_PAGOS.sample
    )
}


Service.all.each { |serv|

  cantidad_cuotas = (1..12).to_a.shuffle[0]
  monto_cuota = serv.monto / cantidad_cuotas

  cantidad_cuotas.times { |n|
    Fee.create(service: serv, nro: n + 1, monto: monto_cuota, vencimiento: serv.fecha.+(n.months))
  }

  cantidad_cuotas = serv.fees.count
  cantidad_cuotas -= (cantidad_cuotas - (0..cantidad_cuotas).to_a.sample)
  serv.fees.each { |fee|
    next if cantidad_cuotas != 0
    FeePayment.create(
      fee: fee, 
      debt_collector: serv.customer.debt_collector, 
      monto: fee.monto, 
      fecha_pago: fee.vencimiento.-(3.days)
    )
    cantidad_cuotas -= 1
  }
}